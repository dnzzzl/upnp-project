import socket
from library_upnp import *
import requests
from xml.dom import minidom
from xml.etree import ElementTree as ET

def main():
    #udpsocket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    #location = SSDP.GetLocation(udpsocket=udpsocket)
    location = "http://192.168.1.170:55003/description.xml"
    #location = "http://192.168.1.1:80/RootDevice.xml"
    print("XML Location:", location)
    request = HTTP.GET(location)
    xml = request.text
    status_code = request.status_code
    if status_code == 200 and "schemas-upnp-org" in request.text:
        xml = minidom.parseString(xml)  # returns minidom document
        
    else:
        print("(!)Description could not be identified")
        print("status code:", status_code)
        print(request.text)
        exit()

    urlBase = SCPD.GetUrlBase(xml,location)

    servicesList = SCPD.GetServices(xml)
    if servicesList:
        selectedService = SelectService(servicesList)
    else:
        print("Services NULL")
        exit()

    selectedAction = SelectAction(urlBase,selectedService)
    for arg in selectedAction.inArgs:  # populates the in arguments
        # create method and implement argument types
        value = input(arg.ToString() + ": ")
        selectedAction.arguments.update({arg: value})

    url = urlBase + selectedService.ctrlURL
    soapaction = "\"{0}#{1}\"".format(selectedService.seviceType, selectedAction.name)
    body = GenXMLbody(selectedService, selectedAction)
    headers = {
        'HOST': urlBase,
        'CONTENT-TYPE': 'text/xml; charset=\"utf-8\"',
        "CONNECTION": "close",
        'SOAPACTION': soapaction
    }

    s = requests.Session()
    s.headers = headers
    s.verify = False  # without this we get a connection reset error
    r = s.post(url, data=body)
    responsexml = ET.fromstring(r.content)
    
    print(ET.tostring(responsexml))

if __name__ == "__main__":
    main()